import sys
import re

# Get filename from command line
if len(sys.argv) < 3:
    raise ValueError("Incorrect number of arguments!")

filename = sys.argv[1]
outputFilename = sys.argv[2]

# Open file
with open(filename, 'r') as f:
    # Read all file data
    read_data = f.read()

    # Split by magic phrase
    studentTokens = read_data.split('Number\tStudent Name')

    # Open output file
    with open(outputFilename, 'w') as outfile:
        outfile.write("LAST_NAME,FIRST_NAME,NUMBER,FIRST_MAJOR,SECOND_MAJOR,\n")

        index = 0
        for token in studentTokens:
            print "TOKEN",index

            if not index == 0:
                # Split by newlines
                lineList = token.split('\n')
                # Get second line
                nameLine = lineList[1]
                # Split by tabs
                nameList = nameLine.split("\t")
                # Get name and U number
                studentName = nameList[1]
                studentNumber = nameList[2]

                print studentName
                print studentNumber
                outfile.write(studentName + "," + studentNumber + ",")

                # Find major(s)
                majorList = re.findall(r'^Major[ a-zA-Z]*:[\t](.*)', token, flags=re.MULTILINE)
                print majorList

                firstMajor = majorList[0].split(",")[0]

                outfile.write(firstMajor + ",")
                if len(majorList) > 1:
                    secondMajor = majorList[1].split(",")[0]
                    outfile.write(secondMajor + ",")
                else:
                    outfile.write(",")

                outfile.write('\n')

            index += 1


